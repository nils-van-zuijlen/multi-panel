#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use embassy_executor::Spawner;
use embassy_time::Delay;
use embassy_rp::gpio::{Level, Output, Input, Pull};
use panic_halt as _;
use embassy_net::udp::{PacketMetadata, UdpSocket};
use embassy_net::{Stack, StackResources};
use embassy_net_wiznet::chip::W5500;
use embassy_net_wiznet::*;
use embassy_rp::clocks::RoscRng;
use static_cell::StaticCell;
use embedded_hal_bus::spi::ExclusiveDevice;
use embassy_rp::peripherals::{PIN_17, PIN_20, PIN_21, SPI0};
use embassy_rp::spi::{Async, Config as SpiConfig, Spi};
use embassy_futures::yield_now;
use rand::RngCore;


use tiny_artnet::Art;


#[embassy_executor::task]
async fn ethernet_task(
    runner: Runner<
        'static,
        W5500,
        ExclusiveDevice<Spi<'static, SPI0, Async>, Output<'static, PIN_17>, Delay>,
        Input<'static, PIN_21>,
        Output<'static, PIN_20>,
    >,
) -> ! {
    runner.run().await
}

#[embassy_executor::task]
async fn net_task(stack: &'static Stack<Device<'static>>) -> ! {
    stack.run().await
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    let p = embassy_rp::init(Default::default());
    let mut rng = RoscRng;

    let mut spi_cfg = SpiConfig::default();
    spi_cfg.frequency = 50_000_000;
    let (miso, mosi, clk) = (p.PIN_16, p.PIN_19, p.PIN_18);
    let spi = Spi::new(p.SPI0, clk, mosi, miso, p.DMA_CH0, p.DMA_CH1, spi_cfg);
    let cs = Output::new(p.PIN_17, Level::High);
    let w5500_int = Input::new(p.PIN_21, Pull::Up);
    let w5500_reset = Output::new(p.PIN_20, Level::High);

    let mac_addr = [0x02, 0x00, 0x00, 0x00, 0x00, 0x00];
    static STATE: StaticCell<State<8, 8>> = StaticCell::new();
    let state = STATE.init(State::<8, 8>::new());
    let (device, runner) = embassy_net_wiznet::new(
        mac_addr,
        state,
        ExclusiveDevice::new(spi, cs, Delay),
        w5500_int,
        w5500_reset,
    )
    .await;
    spawner.spawn(ethernet_task(runner)).unwrap();

    // Generate random seed
    let seed = rng.next_u64();

    // Init network stack
    static STACK: StaticCell<Stack<Device<'static>>> = StaticCell::new();
    static RESOURCES: StaticCell<StackResources<2>> = StaticCell::new();
    let stack = &*STACK.init(Stack::new(
        device,
        embassy_net::Config::dhcpv4(Default::default()),
        RESOURCES.init(StackResources::<2>::new()),
        seed,
    ));

    // Launch network task
    spawner.spawn(net_task(&stack)).unwrap();

    // Waiting for DHCP...
    let cfg = wait_for_config(stack).await;
    let local_addr = cfg.address.address().0;
    // IP address: local_addr

    // Then we can use it!
    let mut rx_buffer = [0; 4096];
    let mut tx_buffer = [0; 4096];
    let mut rx_meta = [PacketMetadata::EMPTY; 16];
    let mut tx_meta = [PacketMetadata::EMPTY; 16];
    let mut buf = [0; 4096];
    let port = tiny_artnet::PORT;
    loop {
        let mut socket = UdpSocket::new(stack, &mut rx_meta, &mut rx_buffer, &mut tx_meta, &mut tx_buffer);
        socket.bind(port).unwrap();

        loop {
            let (n, from_addr) = socket.recv_from(&mut buf).await.unwrap();
            match tiny_artnet::from_slice(&buf[..n]) {
                Ok(Art::Dmx(dmx)) => {
                    // Do something with the data
                }
                Ok(Art::Sync) => {
                    // println!("RX: ArtSync - Use these to buffer DMX packets and then synchronize the rendering of multiple DMX universes.");
                }
                Ok(Art::Poll(poll)) => {
                    // println!("RX: ArtPoll - Someone is looking for ArtNet nodes. Let's respond to them to make this node discoverable! {:?}", poll);

                    let poll_reply = tiny_artnet::PollReply {
                        ip_address: &local_addr,
                        port,
                        firmware_version: 0x0001,
                        short_name: "Example Node",
                        long_name: "Tiny Artnet Example Node",
                        mac_address: &mac_addr,
                        status1: 0b00010000, // ports set by front panel
                        status2: 0b00000110, // DHCP capable, 15-bit ports
                        // This Node has one port
                        num_ports: 4,
                        // This node has four output channels
                        port_types: &[0b10000000, 0b10000000, 0b10000000, 0b10000000],
                        // Report that data is being output correctly
                        good_output_a: &[0b10000000, 0b10000000, 0b10000000, 0b10000000],
                        swout: &[1, 2, 3, 4],
                        ..Default::default()
                    };

                    let msg_len = poll_reply.serialize(&mut buf);
                    socket.send_to(&buf[..msg_len], from_addr).await.unwrap();
                    // let broadcast: UdpSocket = UdpSocket::bind("0.0.0.0:0").unwrap();
                    // broadcast
                    //     .set_read_timeout(Some(Duration::new(5, 0)))
                    //     .unwrap();
                    // broadcast.set_broadcast(true).unwrap();
                    // broadcast
                    //     .send_to(&buf[..msg_len], "255.255.255.255")
                    //     .unwrap();

                    //println!("TX: Sent ArtPollReply to {:?}: {:?}", from_addr, poll_reply);
                }
                Err(err) => {
                    //
                }
                _ => {}
            }
        }
    }
}

async fn wait_for_config(stack: &'static Stack<Device<'static>>) -> embassy_net::StaticConfigV4 {
    loop {
        if let Some(config) = stack.config_v4() {
            return config.clone();
        }
        yield_now().await;
    }
}
